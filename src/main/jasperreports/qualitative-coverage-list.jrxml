<?xml version="1.0" encoding="UTF-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2010 Henix, henix.fr

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses/>.

-->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="qualitative-coverage-list" pageWidth="776" pageHeight="595" orientation="Landscape" columnWidth="736" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" resourceBundle="/WEB-INF/messages/tm/messages" whenResourceMissingType="Key" uuid="728089f2-759a-4fdb-9f63-690b62df54d1">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="main_title" mode="Opaque" forecolor="#FFFFFF" backcolor="#000000" hAlign="Center" vAlign="Middle" isBold="true"/>
	<style name="underlined_bold_header" hAlign="Center" vAlign="Middle" isBold="true">
		<box>
			<bottomPen lineWidth="0.5" lineStyle="Double"/>
		</box>
	</style>
	<style name="underlined_italic_header" hAlign="Center" vAlign="Middle" isItalic="true">
		<box>
			<bottomPen lineWidth="0.5" lineStyle="Double"/>
		</box>
	</style>
	<style name="bold_header" hAlign="Center" vAlign="Middle" isBold="true">
		<box>
			<bottomPen lineWidth="0.5" lineStyle="Dashed"/>
		</box>
	</style>
	<style name="header" hAlign="Center" vAlign="Middle" isItalic="true"/>
	<style name="red_header" forecolor="#FB3333" hAlign="Center" vAlign="Middle" isItalic="true"/>
	<style name="green_header" forecolor="#009900" hAlign="Center" vAlign="Middle" isItalic="true"/>
	<style name="grey_header" forecolor="#999999" hAlign="Center" vAlign="Middle" fontSize="8" isItalic="true"/>
	<style name="header_without_bar" hAlign="Center" vAlign="Middle" isBold="true">
		<box rightPadding="5"/>
	</style>
	<style name="name" mode="Opaque" forecolor="#FFFFFF" backcolor="#77132E" vAlign="Middle" isBold="true"/>
	<style name="legende" mode="Opaque" forecolor="#000000" backcolor="#F2F2F2" vAlign="Middle" fontSize="8" isBold="true"/>
	<style name="numeric_field" mode="Opaque" forecolor="#000000" backcolor="#F2F2F2" hAlign="Center" vAlign="Middle" fontSize="8" isItalic="true"/>
	<style name="numeric_red_field" mode="Opaque" forecolor="#FB3333" backcolor="#F2F2F2" hAlign="Center" vAlign="Middle" fontSize="8" isItalic="true"/>
	<style name="numeric_yellow_field" mode="Opaque" forecolor="#FFAA00" backcolor="#F2F2F2" hAlign="Center" vAlign="Middle" fontSize="8" isItalic="true"/>
	<style name="numeric_green_field" mode="Opaque" forecolor="#009900" backcolor="#F2F2F2" hAlign="Center" vAlign="Middle" fontSize="8" isItalic="true"/>
	<style name="numeric_bold_field" mode="Opaque" forecolor="#000000" backcolor="#F2F2F2" hAlign="Center" vAlign="Middle" fontSize="8" isBold="true" isItalic="true">
		<box rightPadding="5"/>
	</style>
	<style name="numeric_grey_field" mode="Opaque" forecolor="#999999" backcolor="#F2F2F2" hAlign="Center" vAlign="Middle" fontSize="8" isItalic="true"/>
	<parameter name="subReportListCampaigns" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="subReportListIterations" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="subReportListTestSuites" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="milestoneLabel" class="java.lang.String"/>
	<field name="projectId" class="java.lang.Long"/>
	<field name="projectName" class="java.lang.String"/>
	<field name="campaigns" class="java.util.List"/>
	<title>
		<band height="50">
			<textField isStretchWithOverflow="true">
				<reportElement uuid="d165e3c5-674d-4fef-83c0-2c3885d40196" style="main_title" stretchType="RelativeToBandHeight" x="0" y="0" width="726" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.dashboard} + ( ($P{milestoneLabel} != null) ? "( "+msg($R{title.Milestone}, $P{milestoneLabel})+" )" : "")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="a786a57f-3081-43ef-a244-0f6073de9883" style="legende" stretchType="RelativeToBandHeight" x="0" y="20" width="50" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.legende}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="d5941166-65a0-41c7-bd0b-8cf53da55312" style="numeric_red_field" stretchType="RelativeToBandHeight" x="50" y="20" width="50" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.critical}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="0aded93b-9ed9-4548-8ec3-e21865eaf7aa" style="numeric_yellow_field" stretchType="RelativeToBandHeight" x="100" y="20" width="50" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.major}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="9cddf5b1-6268-4888-99b2-0b0c2808941a" style="numeric_green_field" stretchType="RelativeToBandHeight" x="150" y="20" width="50" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.minor}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="86b65a35-726a-48f8-8b67-a66b90926cc9" style="numeric_grey_field" stretchType="RelativeToBandHeight" x="200" y="20" width="50" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.undefined}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="059ee668-30da-4827-9b89-ab5da35a0286" style="numeric_field" stretchType="RelativeToBandHeight" x="250" y="20" width="50" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.total}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="5c9a3e0d-752e-4aec-8427-1bbf3c229685" style="numeric_field" stretchType="RelativeToBandHeight" x="300" y="20" width="426" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.legende.message}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="50">
			<textField isStretchWithOverflow="true">
				<reportElement uuid="5a1bb770-454c-45ab-925c-3a9a5dd82d27" style="bold_header" stretchType="RelativeToBandHeight" x="206" y="0" width="410" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.nb.covered.requirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="f931b401-1fb6-482b-98da-9ab5d509c4e8" style="underlined_italic_header" stretchType="RelativeToBandHeight" x="206" y="20" width="100" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.not.tested}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="e3251e00-8a62-4d2c-a3a4-0aa5ece0e36e" style="underlined_italic_header" stretchType="RelativeToBandHeight" x="316" y="20" width="100" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.tested}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="d876b089-58ee-4245-b2cc-81cd21fed394" style="underlined_italic_header" stretchType="RelativeToBandHeight" x="426" y="20" width="100" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.total}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="d8fc772d-8d5e-4b5f-85ae-022d377bab30" style="underlined_italic_header" stretchType="RelativeToBandHeight" x="536" y="20" width="80" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.progress}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="864e9572-f91a-4841-802d-0faebd6133cc" style="underlined_bold_header" stretchType="RelativeToBandHeight" x="626" y="20" width="100" height="25">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.validated.requirements}]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="20">
			<textField isStretchWithOverflow="true">
				<reportElement uuid="ea0710e0-a862-4e12-834d-27dbc25243a2" style="name" stretchType="RelativeToBandHeight" x="0" y="0" width="726" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement/>
				<textFieldExpression><![CDATA[msg($R{report.qualitativecoverage.label.project},$F{projectName})]]></textFieldExpression>
			</textField>
		</band>
		<band height="20">
			<subreport>
				<reportElement uuid="0605f4c7-d57c-4161-ba7f-2e74ee32f9ea" stretchType="RelativeToTallestObject" x="0" y="0" width="776" height="20"/>
				<subreportParameter name="subReportListCampaigns">
					<subreportParameterExpression><![CDATA[$P{subReportListCampaigns}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="subReportListIterations">
					<subreportParameterExpression><![CDATA[$P{subReportListIterations}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="subReportListTestSuites">
					<subreportParameterExpression><![CDATA[$P{subReportListTestSuites}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="REPORT_RESOURCE_BUNDLE">
					<subreportParameterExpression><![CDATA[$P{REPORT_RESOURCE_BUNDLE}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($F{campaigns})]]></dataSourceExpression>
				<subreportExpression><![CDATA[$P{subReportListCampaigns}]]></subreportExpression>
			</subreport>
		</band>
	</detail>
</jasperReport>
