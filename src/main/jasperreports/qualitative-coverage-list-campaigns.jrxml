<?xml version="1.0" encoding="UTF-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2010 Henix, henix.fr

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses/>.

-->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="qualitative-coverage-list-campaigns" pageWidth="776" pageHeight="595" orientation="Landscape" columnWidth="736" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" resourceBundle="/WEB-INF/messages/tm/messages" whenResourceMissingType="Key">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="name" mode="Opaque" forecolor="#000000" backcolor="#BFE1FF" vAlign="Middle"/>
	<style name="numeric_field" mode="Opaque" forecolor="#000000" backcolor="#BFE1FF" hAlign="Center" vAlign="Middle"/>
	<style name="numeric_red_field" mode="Opaque" forecolor="#000000" backcolor="#BFE1FF" hAlign="Center" vAlign="Middle"/>
	<style name="numeric_green_field" mode="Opaque" forecolor="#000000" backcolor="#BFE1FF" hAlign="Center" vAlign="Middle"/>
	<style name="numeric_yellow_field" mode="Opaque" forecolor="#000000" backcolor="#BFE1FF" hAlign="Center" vAlign="Middle"/>
	<style name="numeric_bold_field" mode="Opaque" forecolor="#000000" backcolor="#BFE1FF" hAlign="Center" vAlign="Middle"/>
	<style name="numeric_grey_field" mode="Opaque" forecolor="#000000" backcolor="#BFE1FF" hAlign="Center" vAlign="Middle"/>
	<parameter name="subReportListIterations" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="subReportListTestSuites" class="net.sf.jasperreports.engine.JasperReport"/>
	<field name="campaignName" class="java.lang.String"/>
	<field name="iterations" class="java.util.List"/>
	<field name="numberOfNontestedCriticalRequirements" class="java.lang.Long"/>
	<field name="numberOfTestedCriticalRequirements" class="java.lang.Long"/>
	<field name="totalNumberOfCriticalRequirements" class="java.lang.Long"/>
	<field name="numberOfVerifiedCriticalRequirements" class="java.lang.Long"/>
	<field name="numberOfNontestedMajorRequirements" class="java.lang.Long"/>
	<field name="numberOfTestedMajorRequirements" class="java.lang.Long"/>
	<field name="totalNumberOfMajorRequirements" class="java.lang.Long"/>
	<field name="numberOfVerifiedMajorRequirements" class="java.lang.Long"/>
	<field name="numberOfNontestedMinorRequirements" class="java.lang.Long"/>
	<field name="numberOfTestedMinorRequirements" class="java.lang.Long"/>
	<field name="totalNumberOfMinorRequirements" class="java.lang.Long"/>
	<field name="numberOfVerifiedMinorRequirements" class="java.lang.Long"/>
	<field name="numberOfNontestedUndefinedRequirements" class="java.lang.Long"/>
	<field name="numberOfTestedUndefinedRequirements" class="java.lang.Long"/>
	<field name="totalNumberOfUndefinedRequirements" class="java.lang.Long"/>
	<field name="numberOfVerifiedUndefinedRequirements" class="java.lang.Long"/>
	<field name="numberOfNontestedRequirements" class="java.lang.Long"/>
	<field name="numberOfTestedRequirements" class="java.lang.Long"/>
	<field name="totalNumberOfRequirements" class="java.lang.Long"/>
	<field name="percentageOfProgress" class="java.lang.Long"/>
	<field name="totalNumberOfVerifiedRequirements" class="java.lang.Long"/>
	<detail>
		<band height="20">
			<textField isStretchWithOverflow="true">
				<reportElement style="name" stretchType="RelativeToBandHeight" x="0" y="0" width="200" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[msg($R{report.qualitativecoverage.label.campaign},$F{campaignName})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_red_field" stretchType="RelativeToBandHeight" x="206" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfNontestedCriticalRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_yellow_field" stretchType="RelativeToBandHeight" x="226" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfNontestedMajorRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_green_field" stretchType="RelativeToBandHeight" x="246" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfNontestedMinorRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_grey_field" stretchType="RelativeToBandHeight" x="266" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfNontestedUndefinedRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_field" stretchType="RelativeToBandHeight" x="286" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfNontestedRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_red_field" stretchType="RelativeToBandHeight" x="316" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfTestedCriticalRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_yellow_field" stretchType="RelativeToBandHeight" x="336" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfTestedMajorRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_green_field" stretchType="RelativeToBandHeight" x="356" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfTestedMinorRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_grey_field" stretchType="RelativeToBandHeight" x="376" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfTestedUndefinedRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_field" stretchType="RelativeToBandHeight" x="396" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfTestedRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_red_field" stretchType="RelativeToBandHeight" x="426" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{totalNumberOfCriticalRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_yellow_field" stretchType="RelativeToBandHeight" x="446" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{totalNumberOfMajorRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_green_field" stretchType="RelativeToBandHeight" x="466" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{totalNumberOfMinorRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_grey_field" stretchType="RelativeToBandHeight" x="486" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{totalNumberOfUndefinedRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_field" stretchType="RelativeToBandHeight" x="506" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{totalNumberOfRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_field" stretchType="RelativeToBandHeight" x="536" y="0" width="80" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{percentageOfProgress}+"%"]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_red_field" stretchType="RelativeToBandHeight" x="626" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfVerifiedCriticalRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_yellow_field" stretchType="RelativeToBandHeight" x="646" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfVerifiedMajorRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_green_field" stretchType="RelativeToBandHeight" x="666" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfVerifiedMinorRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_grey_field" stretchType="RelativeToBandHeight" x="686" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{numberOfVerifiedUndefinedRequirements}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="numeric_field" stretchType="RelativeToBandHeight" x="706" y="0" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{totalNumberOfVerifiedRequirements}]]></textFieldExpression>
			</textField>
		</band>
		<band height="20">
			<subreport>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="776" height="20"/>
				<subreportParameter name="subReportListIterations">
					<subreportParameterExpression><![CDATA[$P{subReportListIterations}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="subReportListTestSuites">
					<subreportParameterExpression><![CDATA[$P{subReportListTestSuites}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="REPORT_RESOURCE_BUNDLE">
					<subreportParameterExpression><![CDATA[$P{REPORT_RESOURCE_BUNDLE}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($F{iterations})]]></dataSourceExpression>
				<subreportExpression class="net.sf.jasperreports.engine.JasperReport"><![CDATA[$P{subReportListIterations}]]></subreportExpression>
			</subreport>
		</band>
	</detail>
</jasperReport>
