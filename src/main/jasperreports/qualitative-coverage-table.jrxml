<?xml version="1.0" encoding="UTF-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2010 Henix, henix.fr

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses/>.

-->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="qualitative-coverage-table" pageWidth="776" pageHeight="595" orientation="Landscape" columnWidth="736" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" resourceBundle="/WEB-INF/messages/tm/messages" whenResourceMissingType="Key" uuid="0eaed303-c4bb-497d-8a1b-c20b6eb9e4c3">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="underlined_header" hAlign="Center" vAlign="Middle" fontSize="8" isItalic="true">
		<pen lineWidth="0.5"/>
		<box>
			<bottomPen lineWidth="0.5"/>
		</box>
	</style>
	<style name="underlined_header_vertical" hAlign="Center" vAlign="Middle" rotation="Left" fontSize="8" isItalic="true">
		<pen lineWidth="0.5"/>
		<box>
			<bottomPen lineWidth="0.5"/>
		</box>
	</style>
	<parameter name="subReportTableCampaigns" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="subReportTableIterations" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="subReportTableTestSuites" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="subReportTableRequirements" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="milestoneLabel" class="java.lang.String"/>
	<field name="projectId" class="java.lang.Long"/>
	<field name="projectName" class="java.lang.String"/>
	<field name="requirements" class="java.util.List"/>
	<field name="campaigns" class="java.util.List"/>
	<field name="allowsSettled" class="java.lang.Boolean"/>
	<field name="allowsUntestable" class="java.lang.Boolean"/>
	<variable name="rowcount" class="java.util.Map">
		<variableExpression><![CDATA[new java.util.HashMap()]]></variableExpression>
		<initialValueExpression><![CDATA[new java.util.HashMap()]]></initialValueExpression>
	</variable>
	<columnHeader>
		<band height="65">
			<textField isStretchWithOverflow="true">
				<reportElement uuid="e7a4305d-c207-4e19-930f-2f36ba3124a9" style="underlined_header" stretchType="RelativeToBandHeight" x="547" y="0" width="189" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.nb.testcases}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="0b55c810-7c28-4260-b431-6d1696191fac" style="underlined_header" stretchType="RelativeToBandHeight" x="0" y="20" width="99" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.project2}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="10b54146-b671-4bf6-8fad-6e5b63888ade" style="underlined_header" stretchType="RelativeToBandHeight" x="99" y="20" width="99" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.campaign2}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="2e646b93-429f-4ff3-bdef-1500722d9bc0" style="underlined_header" stretchType="RelativeToBandHeight" x="198" y="20" width="99" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.short.iteration2}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="214034d1-b36e-4761-a660-def6bf193b73" style="underlined_header" stretchType="RelativeToBandHeight" x="297" y="20" width="100" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.suite2}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="b0e5512a-f409-45f7-8cda-a48d465e0c81" style="underlined_header" stretchType="RelativeToBandHeight" x="397" y="20" width="100" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.requirement}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="45749a62-20df-4a28-9a8c-6d1505ca088b" style="underlined_header" stretchType="RelativeToBandHeight" x="497" y="20" width="50" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.criticality}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="d360a9e2-9027-4cf0-b4be-e50c7fb3a43b" style="underlined_header_vertical" stretchType="RelativeToBandHeight" x="547" y="20" width="27" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
					<property name="net.sf.jasperreports.export.html.class" value="vertical-text"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.ok}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="5a48c7fa-c54f-4176-ad80-d23380f8e91b" style="underlined_header_vertical" stretchType="RelativeToBandHeight" x="574" y="20" width="27" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
					<property name="net.sf.jasperreports.export.html.class" value="vertical-text"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.settled}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="8e593a71-a590-4ae1-93bf-a122bef442dd" style="underlined_header_vertical" stretchType="RelativeToBandHeight" x="601" y="20" width="27" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
					<property name="net.sf.jasperreports.export.html.class" value="vertical-text"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.ko}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="14b5fc62-5d91-425b-9cdf-4bdc26484562" style="underlined_header_vertical" stretchType="RelativeToBandHeight" x="628" y="20" width="27" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
					<property name="net.sf.jasperreports.export.html.class" value="vertical-text"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.blocked}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="5cbe8dd7-5d8b-4df7-8ef5-e6010756f34d" style="underlined_header_vertical" stretchType="RelativeToBandHeight" x="655" y="20" width="27" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
					<property name="net.sf.jasperreports.export.html.class" value="vertical-text"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.untestable}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="3d8dfefb-8d2b-430b-b866-64b3d5928671" style="underlined_header_vertical" stretchType="RelativeToBandHeight" x="682" y="20" width="27" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
					<property name="net.sf.jasperreports.export.html.class" value="vertical-text"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.running}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="a92513ef-e2a6-45bf-ae63-64399ad22693" style="underlined_header_vertical" stretchType="RelativeToBandHeight" x="709" y="20" width="27" height="45">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
					<property name="net.sf.jasperreports.export.html.class" value="vertical-text"/>
				</reportElement>
				<box leftPadding="5"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$R{report.qualitativecoverage.label.ready}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="8e5618e7-a935-4e7d-9ea4-b39beff8eb57" x="0" y="0" width="100" height="20">
					<printWhenExpression><![CDATA[($P{milestoneLabel}!=null)]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="13" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[msg($R{title.Milestone}, $P{milestoneLabel})]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="20">
			<printWhenExpression><![CDATA[( $V{rowcount}.put("rowcount", Integer.valueOf(0))!= null || java.lang.Boolean.TRUE)]]></printWhenExpression>
			<subreport>
				<reportElement uuid="c041a006-6825-4788-a6fb-f0e29148b33e" stretchType="RelativeToTallestObject" x="0" y="0" width="736" height="20"/>
				<subreportParameter name="project">
					<subreportParameterExpression><![CDATA[$F{projectName}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="allowsSettled">
					<subreportParameterExpression><![CDATA[$F{allowsSettled}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="allowsUntestable">
					<subreportParameterExpression><![CDATA[$F{allowsUntestable}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="rowcount">
					<subreportParameterExpression><![CDATA[$V{rowcount}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="subReportTableRequirements">
					<subreportParameterExpression><![CDATA[$P{subReportTableRequirements}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="subReportTableCampaigns">
					<subreportParameterExpression><![CDATA[$P{subReportTableCampaigns}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="subReportTableTestSuites">
					<subreportParameterExpression><![CDATA[$P{subReportTableTestSuites}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="subReportTableIterations">
					<subreportParameterExpression><![CDATA[$P{subReportTableIterations}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="REPORT_RESOURCE_BUNDLE">
					<subreportParameterExpression><![CDATA[$P{REPORT_RESOURCE_BUNDLE}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($F{campaigns})]]></dataSourceExpression>
				<subreportExpression><![CDATA[$P{subReportTableCampaigns}]]></subreportExpression>
			</subreport>
		</band>
	</detail>
</jasperReport>
