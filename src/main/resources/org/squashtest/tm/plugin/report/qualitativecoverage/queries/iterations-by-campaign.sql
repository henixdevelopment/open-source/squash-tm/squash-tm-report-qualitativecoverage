SELECT DISTINCT
	citer.iteration_id, 
	iter.name, 
	citer.iteration_order,
	citer.campaign_id
FROM CAMPAIGN_ITERATION AS citer
JOIN ITERATION AS iter ON citer.iteration_id = iter.iteration_id
WHERE citer.campaign_id = :id
ORDER BY citer.iteration_order ASC
