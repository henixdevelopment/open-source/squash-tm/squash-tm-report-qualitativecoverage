SELECT DISTINCT
	pro.PROJECT_ID as ID,
	pro.NAME as NAME
FROM PROJECT AS pro
join MILESTONE_BINDING mbinding on mbinding.PROJECT_ID = pro.PROJECT_ID
join MILESTONE m on m.MILESTONE_ID = mbinding.MILESTONE_ID
where pro.PROJECT_TYPE = 'P'
and m.MILESTONE_ID in :milestoneIds