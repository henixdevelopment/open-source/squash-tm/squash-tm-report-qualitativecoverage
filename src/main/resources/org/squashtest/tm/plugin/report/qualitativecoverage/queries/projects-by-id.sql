SELECT DISTINCT
	pro.PROJECT_ID as ID,
	pro.NAME as NAME
FROM PROJECT AS pro
WHERE pro.PROJECT_ID = :id
AND pro.PROJECT_TYPE = 'P'