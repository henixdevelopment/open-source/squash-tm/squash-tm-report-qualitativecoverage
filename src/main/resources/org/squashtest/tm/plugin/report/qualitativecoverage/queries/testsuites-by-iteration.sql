SELECT DISTINCT
	its.test_suite_id, 
	ts.name,
	its.iteration_id
FROM ITERATION_TEST_SUITE AS its
JOIN TEST_SUITE AS ts ON ts.id = its.test_suite_id
WHERE its.iteration_id = :id