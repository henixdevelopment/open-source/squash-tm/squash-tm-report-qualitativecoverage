<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>org.squashtest.tm.plugin</groupId>
  <artifactId>report.qualitativecoverage</artifactId>
  <version>${revision}${sha1}${changelist}</version>
  <description>Standard (default) reports for Squash TM</description>
  <name>report-qualitativecoverage</name>

  <properties>
    <!-- PROJECT -->
    <revision>10.0.0</revision>
    <sha1></sha1>
    <changelist>-SNAPSHOT</changelist>
    <java.version>17</java.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

    <!-- DEPENDENCIES -->
    <squash.core.version>10.0.0-SNAPSHOT</squash.core.version>

    <jasperreports.version>6.21.3</jasperreports.version>
    <jasperreports.build.outputDirectory>${project.build.outputDirectory}/jasperreports</jasperreports.build.outputDirectory>

    <!-- MAVEN PLUGINS -->
    <ci-friendly.version>1.0.20</ci-friendly.version>
    <jasperreports-plugin.version>2.8</jasperreports-plugin.version>
    <license-maven-plugin.version>4.5</license-maven-plugin.version>
    <maven-assembly-plugin.version>3.7.1</maven-assembly-plugin.version>
    <maven-compiler-plugin.version>3.13.0</maven-compiler-plugin.version>
    <maven-release-plugin.version>3.1.1</maven-release-plugin.version>
    <xml-apis.version>1.4.01</xml-apis.version>

    <!-- SONAR -->
    <sonar.projectKey>${project.groupId}:${project.artifactId}${env.APPEND_SONAR_PROJECT_KEY}</sonar.projectKey>
    <sonar.projectName>${project.name}${env.APPEND_SONAR_PROJECT_NAME}</sonar.projectName>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.squashtest.tm</groupId>
        <artifactId>squash-tm-bom</artifactId>
        <version>${squash.core.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <!--======= SQUASHTEST MODULES ======== -->
    <dependency>
      <groupId>org.squashtest.tm</groupId>
      <artifactId>core.api</artifactId>
      <version>${squash.core.version}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>org.squashtest.tm</groupId>
      <artifactId>core.report.api</artifactId>
      <version>${squash.core.version}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>net.sf.jasperreports</groupId>
      <artifactId>jasperreports</artifactId>
      <version>${jasperreports.version}</version>
      <scope>provided</scope>
      <optional>true</optional>
    </dependency>

    <dependency>
      <groupId>org.squashtest.tm</groupId>
      <artifactId>tm.service</artifactId>
      <version>${squash.core.version}</version>
      <scope>provided</scope>
    </dependency>
  </dependencies>

  <build>
    <resources>
      <resource>
        <filtering>true</filtering>
        <directory>src/main/resources</directory>
      </resource>
    </resources>

    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-release-plugin</artifactId>
          <version>${maven-release-plugin.version}</version>
        </plugin>
      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>${maven-compiler-plugin.version}</version>
        <configuration>
          <source>${java.version}</source>
          <target>${java.version}</target>
        </configuration>
      </plugin>

      <plugin>
        <groupId>com.alexnederlof</groupId>
        <artifactId>jasperreports-plugin</artifactId>
        <version>${jasperreports-plugin.version}</version>
        <configuration>
          <outputDirectory>${jasperreports.build.outputDirectory}</outputDirectory>
        </configuration>
        <dependencies>
          <dependency>
            <!-- required to make sure jr runtime version is in sync with compile-time version -->
            <groupId>net.sf.jasperreports</groupId>
            <artifactId>jasperreports</artifactId>
            <version>${jasperreports.version}</version>
            <exclusions>
              <exclusion>
                <!-- without this, old 1.3 version may be pulled outta nowhere and warious problems occur -->
                <groupId>xml-apis</groupId>
                <artifactId>xml-apis</artifactId>
              </exclusion>
            </exclusions>
          </dependency>
          <dependency>
            <!-- without this, old 1.3 version may be pulled outta nowhere and reports wont compile -->
            <groupId>xml-apis</groupId>
            <artifactId>xml-apis</artifactId>
            <version>${xml-apis.version}</version>
          </dependency>
        </dependencies>
        <executions>
          <execution>
            <goals>
              <goal>jasper</goal>
            </goals>
            <phase>prepare-package</phase>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>com.outbrain.swinfra</groupId>
        <artifactId>ci-friendly-flatten-maven-plugin</artifactId>
        <version>${ci-friendly.version}</version>
        <executions>
          <execution>
            <goals>
              <!-- Ensure proper cleanup. Will run on clean phase-->
              <goal>clean</goal>
              <!-- Enable ci-friendly version resolution. Will run on process-resources phase-->
              <goal>flatten</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <!-- Checks license headers throughout the project -->
        <groupId>com.mycila</groupId>
        <artifactId>license-maven-plugin</artifactId>
        <version>${license-maven-plugin.version}</version>
        <inherited>false</inherited>
        <configuration>
          <aggregate>true</aggregate>
          <strictCheck>true</strictCheck>
          <properties>
            <license.copyrightOwner>${project.organization.name}</license.copyrightOwner>
            <license.yearSpan>${project.inceptionYear}</license.yearSpan>
          </properties>
          <licenseSets>
            <licenseSet>
              <header>src/material/header.txt</header>
              <excludes>
                <exclude>template.mf</exclude>
                <exclude>**/pom.xml</exclude>
                <exclude>**/pom.xml.*</exclude>
                <exclude>**/.ci-friendly-pom.xml</exclude>
                <exclude>.settings</exclude>
                <exclude>build.properties</exclude>
                <exclude>.hgignore</exclude>
                <exclude>**/*.sql</exclude>
                <exclude>.hgtags</exclude>
                <exclude>.classpath</exclude>
                <exclude>.project</exclude>
                <exclude>.editorconfig</exclude>
                <exclude>**/material/**</exclude>
                <exclude>META-INF/**</exclude>
                <exclude>.m2/**</exclude>
              </excludes>
            </licenseSet>
          </licenseSets>
          <mapping>
            <tag>DYNASCRIPT_STYLE</tag>
            <jrxml>XML_STYLE</jrxml>
          </mapping>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>check</goal>
            </goals>
            <phase>validate</phase>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
        <version>${maven-assembly-plugin.version}</version>
        <executions>
          <execution>
            <phase>verify</phase>
            <goals>
              <goal>single</goal>
            </goals>
            <configuration>
              <descriptors>
                <descriptor>src/main/assembly/assembly.xml</descriptor>
              </descriptors>
              <appendAssemblyId>false</appendAssemblyId>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
  <inceptionYear>2010</inceptionYear>
  <organization>
    <name>Henix, henix.fr</name>
    <url>http://www.squashtest.org</url>
  </organization>
  <scm>
    <connection>scm:git:${project.basedir}</connection>
    <developerConnection>scm:git:${project.basedir}</developerConnection>
    <tag>HEAD</tag>
  </scm>

  <repositories>
    <repository>
      <id>squash</id>
      <url>http://repo.squashtest.org/maven2/releases</url>
    </repository>
    <repository>
      <id>squash-snapshots</id>
      <url>http://repo.squashtest.org/maven2/snapshots</url>
    </repository>
  </repositories>

  <distributionManagement>
    <repository>
      <id>squash-release-deploy-repo</id>
      <name>Squash releases deployment repo</name>
      <url>${deploy-repo.release.url}</url>
    </repository>
    <snapshotRepository>
      <id>squash-snapshot-deploy-repo</id>
      <name>Squash snapshot deployment repo</name>
      <url>${deploy-repo.snapshot.url}</url>
    </snapshotRepository>
  </distributionManagement>

</project>
